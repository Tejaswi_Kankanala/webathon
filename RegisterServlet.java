import java.io.IOException;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public RegisterServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		response.setContentType("text/html");
		PrintWriter out =response.getWriter();
		String name=request.getParameter("nm");
		String id=request.getParameter("id");
		String pass=request.getParameter("pass");
		String email=request.getParameter("em");
		Connection con=null;
		PreparedStatement stmt=null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","tej123");
			System.out.println("connection established");
		    stmt=con.prepareStatement("insert into login values(?,?,?,?)");
		    
		    stmt.setString(1,name);
		    stmt.setString(2, pass);
		    stmt.setString(3, id);
		    stmt.setString(4, email);
		    stmt.executeUpdate();
		   
	         
		    out.println("<html><body><script>alert('successfully inserted')</script></body></html>;");
        
		    HttpSession session=request.getSession();  
	        session.setAttribute("session_id",id);  
		    
		    response.sendRedirect("index.html");
		}
		catch(Exception e) {
			out.println("nO");
		}
	}

}