import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import jakarta.servlet.RequestDispatcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  response.setContentType("text/html");
	      PrintWriter pr  = response.getWriter();
	      String id = req.getParameter("stu_id");
	      String passw  =req.getParameter("passwr");
	      Connection con = null;
	      Statement st = null;
	      ResultSet rs = null;
	      Boolean b = false;
	      try {
	    	 Class.forName("oracle.jdbc.driver.OracleDriver");
	    	 con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","tej123");
	    	 System.out.println("connection established");
	    	 st = con.createStatement();
	    	 rs = st.executeQuery("select * from login");
	    	 
	    	 while(rs.next()) {
	    		 String rsid = rs.getString(3);
	    		 String passr = rs.getString(2);
	    		 
	    		// pr.println(dname + " "+passr);
	    		 if(id.equals(rsid) && passw.equals(passr)) {
	    			 b = true;
	    			 break;
	    			
	    		 }
	    	 }
	    	  String name = rs.getString(1);
	    	 if(b){
	    		 pr.println("<h3 color='red'>welcome " + name + "</h3>");
	    		 pr.println("<br><a href ='logout'>logout</a>");
	    	 }
	    	 else {
	    		 response.sendRedirect("Register.html");
	                pr.println("please enter valid creadentials");
	    		 }
	    	 
	    	 HttpSession session  = req.getSession();
	 		if(session!= null) {
	 		
	 			//long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);
	 			
	 			
	 			
	 			 long start_access  = session.getCreationTime();
	 			 long last_access = session.getLastAccessedTime();
	 			 
	 	        // Creating date format
	 	        DateFormat simple = new SimpleDateFormat(
	 	            "dd MMM yyyy HH:mm:ss:SSS Z");
	 	 
	 	        // Creating date from milliseconds
	 	        // using Date() constructor
	 	        Date result = new Date(start_access);
	 	 
	 	        // Formatting Date according to the
	 	        // given format
	 	        pr.println("<br>CREATION TIME: "+simple.format(result));
	 	        
	 	        pr.println("<br>LAST ACCESSED TIME: "+simple.format(new Date(last_access))); 
	 		}
	      }catch(Exception e) {
	    	  e.printStackTrace();
	      }
	      
	      
	      
		
	}

	

}